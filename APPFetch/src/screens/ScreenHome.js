import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  ListItem,
  Button,
} from 'react-native';
//import {List} from 'react-native-paper';
import BuscaCancion from '../components/BuscaCancion';
import CancionItem from '../components/CancionItem';
import {getNewsCancionesAPI} from '../api/peticiones';
import {map} from 'lodash';
import axios from 'axios';
import ServiceCanciones from '../api/serviceCanciones';
import API_HOST_CANCIONES from '../utils/constants';
import {TextInput} from 'react-native-gesture-handler';
import Navigation from '../navigation/Navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';

/*
const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
];
*/

/*
const Item = ({title}) => (
  <View style={styles.item}>
    <Text style={styles.title}>{title}</Text>
  </View>
);
*/


export default function ScreenHome(props) {
  const {navigation} = props;
  const [formData, setFormData] = useState(null);
  //console.log(props.route);
  const [usuario, setUsuario] = useState(null);
  const [nombreCanciones, setNombreCanciones] = useState('');
  const [monedas, setMonedas] = useState(null);

  //Si sirve
  /*
    useEffect( () => {
        const consultarAPI = async () =>{
            const url ='https://api.lyrics.ovh/suggest/black';
            const resultado= await axios.get(url);
            console.log(resultado.data.data);
            setMonedas(resultado.data.data);
            console.log("monedas"+monedas);
        }
        consultarAPI();
    }, []
    );
*/


 useEffect(() => {
  // Actualiza el título del documento usando la API del navegador    document.title = `You clicked ${count} times`;  
  getData(formData);
},[formData]);


  useEffect(() => {
    getNewsCancionesAPI(nombreCanciones.toString()).then((response) => {
      //setCanciones(response.results);
      setMonedas(response.data);
      //console.log(response.data);
      //console.log("Aqui 1");
    });

    //getData();
  }, [nombreCanciones]);
  //
  //Cuenta: {props.route.params.email}
  //

  /*
const [nombreCanciones, setNombreCanciones]=useState("queen");

useEffect( () => {
    const consultarAPI = async () =>{
        const songNameEncoded = encodeURI("queen");
        const url = `${API_HOST_CANCIONES}/suggest/${songNameEncoded}`;

        try {    
            const resultado= await axios.get(url);
            console.log(resultado.data.data);
            setMonedas(resultado.data.data);
            console.log("monedas kkk"+monedas);
            
        } catch (error) {
            console.log({error});
        }
    }
    consultarAPI();
}, []
);

/*
    useEffect(  () => {
            const consultarAPI =async () =>{
            const resultado= await ServiceCanciones("queen");
            console.log(resultado.data.data);
            setMonedas(resultado.data.data);
            console.log("monedas kkk"+monedas);
    }
            consultarAPI();        
    }, []
    );
*/

  /*
useEffect( () => {
    const consultarAPI = async () =>{
        //const songNameEncoded = encodeURI("queen");

        try {    
        const url = `${API_HOST_CANCIONES}/suggest/queen`;
            const resultado= await axios.get(url);
            console.log(resultado.data.data);
            setMonedas(resultado.data.data);
            console.log("monedas kkk"+monedas);
            
        } catch (error) {
            console.log({error});
        }
    }
    consultarAPI();
}, []
);
*/

  /*
    const renderItem = ({ item }) => (
        <Item title={ `${item.title} -> ${item.artist.name} -> ${item.album.title} -> ${item.link} ` }
        />

      ); 
*/
  /*
     const renderItem = ({ item }) => (
        <ListItem title={item.title}   
        />

      );
  */

  const goScreenLogin = () => { 
    navigation.replace('Login'); 
    console.log('yendo para login');
  } 

  const logout = async () => {
    try {
      const jsonValue = JSON.stringify(null);
      await AsyncStorage.setItem('LlaveNombre', jsonValue);
      //await AsyncStorage.setItem('LlaveNombre', null)
      console.log('eliminado...');
      goScreenLogin();
      //navigation.replace('Login');
      //irLogin();
    } catch (e) {
      // saving error
      console.log(e);
      console.log('No fue posible elminar cuenta. Try again');
    } finally {
      //navigation.replace('Login');
      //console.log('en finally');
    }
  };

  const getData = async (formData) => {
    try {
      const jsonValue = await AsyncStorage.getItem('LlaveNombre');

      if (jsonValue != null) {
        //console.log('no nulo getData!!!!');
        //console.log('email');
        
        console.log(JSON.parse(jsonValue).email);
        //console.log("--->"+JSON.parse(jsonValue));
        
        //credenciales = JSON.parse(jsonValue);
        console.log("--->"+JSON.parse(jsonValue));
        //let credenciales= JSON.parse(jsonValue);
        //setFormData(JSON.parse(jsonValue));

        console.log(JSON.parse(jsonValue));
        formData=SON.parse(jsonValue);
        return JSON.parse(jsonValue);
      } else {
        //console.log('saliendo');
        console.log('jasonValue es NULO');
        //navigation.replace('Login');
        goScreenLogin();
        return null;
      }
    } catch (e) { 
      console.log('hubo un error al momento de recuperar AsyncStorage');
      console.log('jasonValue es NULO');
      goScreenLogin();
      //navigation.replace('Login');
      return null;
      // error reading value
    }
  };

  /*
const getData = async () => {
  try {
    const value = await AsyncStorage.getItem('LlaveNombre')
    if(value !== null) {
      // value previously stored
      console.log('getData');
      console.log(value);
    } else navigation.navigate("Login");
  } catch(e) {
    // error reading value
    console.log(error);
  }
}
*/

//if (getData() === null) {  goScreenLogin(); return null; } 
//  else {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.cuentaContent}>

          { formData ? 
          <Text style={styles.textTitle}>Cuenta: {formData.email}</Text> : 
          <Text style={styles.textTitle}>Error</Text>}

          <Button
            style={styles.btnLogout}
            title="salir"
            onPress={() => {
              logout();
            }}
            />
        </View>

        <TextInput
          placeholder="Buscar canción"
          style={styles.inputText}
          onChangeText={(val) => setNombreCanciones(val)}></TextInput>

        <FlatList
          data={monedas}
          renderItem={({item}) => (
            <CancionItem item={item} navigation={navigation} />
          )}
          keyExtractor={(item) => item.id.toString()}
        />
      </SafeAreaView>
      );
//    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    marginTop: StatusBar.currentHeight || 0,
    marginVertical: 1,
    marginHorizontal: 8,
  },
  cuentaContent: {
    flexDirection: 'row',
  },
  textTitle: {
    textAlign: 'left',
    fontFamily: 'Cochin',
    padding: 20,
    fontSize: 15,
  },
  btnLogout: {
    textAlign: 'right',
  },
  inputText: {
    backgroundColor: '#fff',
    padding: 20,
    marginVertical: 4,
    marginHorizontal: 16,
    borderRadius: 20,
    fontSize: 20,
  },
});
