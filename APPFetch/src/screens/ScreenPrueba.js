import React, {useState} from 'react';
import {StyleSheet, View, Text, TextInput, Button, TouchableOpacity,Image} from 'react-native';

export default function ScreenInfo(props){
    //const {item}=props

    //const [item]=useState(props.route.params);

    const item=props.route.params;

    console.log(props.route.params);
    //const {item}=props.route.params;
    console.log("1 -->"+item);

    //setItem(props.route.params);
    //console.log("2 -->"+item);

    return(
        <>
            
            <Image
                style={styles.estiloImagen}
                source={ {uri: `${item.album.cover_medium}`} } />

            <Text style={styles.estiloTexto}>
                {item.title} 
            </Text>

            <Text style={styles.estiloTexto}>
                {item.artist.name} 
            </Text>

        </>
    )
}

const styles = StyleSheet.create({
    estiloImagen: {
        alignSelf:'center',
        height: 400,
        width: 300,
        padding: 20,
        marginVertical: 10,
        marginHorizontal: 10,
        borderRadius:30
      },
    estiloTexto:{
        fontSize: 20,
        textAlign: 'center',
        marginHorizontal: 10,
        marginVertical: 10,
    }
});
