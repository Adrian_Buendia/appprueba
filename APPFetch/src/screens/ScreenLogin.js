import React, {useState} from 'react';
import {StyleSheet, View, Text, TextInput, Button, TouchableOpacity} from 'react-native';
import Navigation from '../navigation/Navigation';
import Auth from '../components/Auth';
import RegisterForm from "../components/RegisterForm";
import validations from "../utils/validations";
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function ScreenLogin(props){
    const {navigation} = props;

    const {changeForm}=props;
    const [formData, setFormData] = useState(defaultValue());
    const [formError, setFormError] = useState(false);

    const register= () =>{
        let errors = {};
            if( !formData.email || !formData.password || !formData.repeatPassword ){
                if( !formData.email ) errors.email = true;
                if( !formData.password ) errors.password = true;
                if( !formData.repeatPassord ) errors.repeatPassword = true;

                setFormError(true);
            
            }else if(formData.password !== formData.repeatPassword ){
                    errors.password = true;
                    errors.repeatPassword= true ;
                    setFormError(true);
            }else{
                console.log("exito");
                //formData.email);
                //formData.password);
                //formData.repeatPassword);
                //comprueba=true;
                //console.log(comprueba);
                setFormError(false);
                storeData(formData);
                

                //navigation.navigate("Home", formData);
            }
        //console.log(formData);
        //console.log(errors);

        return formError;
    };


    const storeData = async (formData) => {
        try {
            const jsonValue = JSON.stringify(formData)
            await AsyncStorage.setItem('LlaveNombre', jsonValue)
            console.log(jsonValue);
            console.log("agregando...");
            irHome();
          } catch (e) {
            // saving error
            console.log(e);
            console.log("catch...");
          }

      }

      const irHome= () => {
        navigation.replace("Home");}

    return (

        <View>
            <Text> ScreenLogin </Text>

            <TextInput
                 style={styles.input}
                 placeholder="Correo electrónico" 
                 placeholderTextColor="#969696"
                 onChange={ (e) => setFormData( {...formData, email: e.nativeEvent.text} ) }     
                 />

            <TextInput
                 style={styles.input}
                 placeholder="Contraseña" 
                 placeholderTextColor="#969696"
                 secureTextEntry={true}
                 
                 onChange={ (e) => setFormData( {...formData, password: e.nativeEvent.text} ) }
                 />

            <TextInput
                 style={styles.input}
                 placeholder="Repetir contraseña" 
                 placeholderTextColor="#969696"
                 secureTextEntry={true}

                 onChange={ (e) => setFormData( {...formData, repeatPassword: e.nativeEvent.text} ) }
                 />

            { formError  ? <Text style={styles.textoerror}> Indroducir datos válidos </Text> : <Text >  </Text> }

            <Button
            onPress={   () => register() 
                }
                title="Ir a Home"
                    > </Button>
        </View>
      
    );
}

const styles= StyleSheet.create({
    background:{
      backgroundColor :"#15212b",
      height: "100%"
        },
    
    btnText:{
        color: '#fff',
        fontSize: 18
        },
    
    input:{
        height: 50,
        color: "#fff",
        width: "80%",
        margin: 20,
        backgroundColor: '#1e3040',
        paddingHorizontal: 20,
        borderRadius: 50,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#1e3040',
    },
    
    login:{
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 10,
    },

    textoerror:{
        height: 50,
        color: "#FF0000",
        width: "100%",
        margin: 10,
        paddingHorizontal: 20,
        borderRadius: 50,
        fontSize: 18,
        borderColor: '#1e3040',     
    }

  });

  function defaultValue(){
    return {
        email:'',
        password:'',
        repeatPassword:'',
    };
}

