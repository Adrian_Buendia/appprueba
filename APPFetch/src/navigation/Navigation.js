import React from "react" ;
import {createStackNavigator} from "@react-navigation/stack";
import ScreenHome from "../screens/ScreenHome";
import ScreenInfo from "../screens/ScreenInfo";

import ScreenLogin from "../screens/ScreenLogin";

const Stack =createStackNavigator();

export default function Navigation(){
    return(
        <Stack.Navigator>

            <Stack.Screen name="Home" component={ScreenHome} options={{title: 'Home'}} /> 
<Stack.Screen name="Login" component={ScreenLogin} options={{title: 'Login'} } /> 
            <Stack.Screen name="Info" component={ScreenInfo} options={{title: 'Info'} } /> 
        </Stack.Navigator>
    )
}