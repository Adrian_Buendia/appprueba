import React, { useEffect }  from 'react';
import API_HOST_CANCIONES from '../utils/constants';
import axios from 'axios';

export default function ServiceCanciones(nombreCanciones){

    const songNameEncoded = encodeURI(nombreCanciones);
    const url = `${API_HOST_CANCIONES}/suggest/${songNameEncoded}`;

      //      const url ='https://api.lyrics.ovh/suggest/queen';

      /*
    try {
        return axios.get(url);
    } catch (error) {
        //return error;
    } */

    return axios.get(url);
}