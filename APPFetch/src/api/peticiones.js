import { API_HOST, API_HOST_CANCIONES} from "../utils/constants";

export function getNewsCancionesAPI( nombre ) {
    const songNameEncoded = encodeURI(nombre);
    const url = `${API_HOST_CANCIONES}/suggest/${songNameEncoded}`
    
    //const url = `${API_HOST}/posts`
    //const url = `${API_HOST}`
    
    //fetch(url).then(
    //    (response) => { console.log(response);
    //});

    //fetch(url).then(
    //    (response) => { 
    //        return response.json();
    //    }
    //).then(
    //    (result) => {
    //        console.log(result);
    //    }
    //); 
    
    return fetch(url).then(
        (response) => { 
            return response.json();
        }
    ).then(
        (result) => {
            return result;
        }
        
    ); 

}

