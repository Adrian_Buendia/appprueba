import React from 'react';
import {View,Text,StyleSheet,TouchableOpacity} from 'react-native';

export default function LoginForm(props){
    //console.log(props);

    const {changeForm}=props;

    return(
        <View >       
            <Text> En LoginForm </Text>
            <TouchableOpacity
                onPress={changeForm}>
                <Text style={styles.btnText}>
                    Registrate  
                </Text>
            </TouchableOpacity>
        </View>
    );
}

const styles=StyleSheet.create({

    texto:{
        color:"#FFFFFF"
    },

    btnText:{
        color: '#fff',
        fontSize: 18
    }

});