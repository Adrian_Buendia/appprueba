import React, { useState,useEffect }  from 'react';
import {View,Text,StyleSheet ,Image, Button, TouchableOpacity,TextInput,SafeAreaView} from 'react-native';
import {getNewsCancionesAPI} from '../api/peticiones';
import {map } from 'lodash';
import axios from 'axios';

export default function BuscaCancion(){
    //const [newCanciones, setCanciones] = useState (null);
    const [nombreCancion, setNombreCancion] = useState (null);
    //https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD

    /*
    useEffect(
        () => {  getNewsCancionesAPI("paint it black").then(
          (response) => {
            setCanciones(response.results);
            console.log(response);
            console.log("Aqui 1");
          }
        );
     }, [])
     */

    return(
        <>
            <TextInput 
                style={styles.input}
                placeholder="buscar canción"    
                placeholderTextColor="#000000"
                onChange={ (e) => setNombreCancion( e.nativeEvent.text ) }
                color="#000000"
                >
            </TextInput> 
            
            <Text  >{nombreCancion} </Text>

      

        </>
    );
}

const styles= StyleSheet.create({
    view:{
        flex:1,
        alignItems:"center",
    },

    logo:{
        width:"80%",
        height:"40%",
        marginTop:10,
        marginBottom:10,
    },

    buscador:{
        width:"80%",
        height:"40%",
        marginTop:10,
        marginBottom:10,
        color:"#000000"
    },
   
    background:{
        height: "100%",
        margin: 20,
        fontSize: 30,
        borderColor:"#15212b",
        borderRadius: 20,
      },
   
    input:{
        height: 50,
        width: "80%",
        margin: 25,
        paddingHorizontal: 20,
        borderRadius: 50,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#1e3040',
    },
    salida:{
        height: 20,
        width: "50%",
        margin: 5,
        paddingHorizontal: 10,
        borderRadius: 50,
        fontSize: 5,
        borderWidth: 1,
        margin: 1
    },
});