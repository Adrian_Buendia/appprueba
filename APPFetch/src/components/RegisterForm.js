import React, {useState} from 'react';
import {View,Text,StyleSheet,TouchableOpacity, TextInput,Button} from 'react-native';
import {validateEmail} from "../utils/validations";
import Navigation from '../navigation/Navigation';

export default function RegisterForm(props){
    const {changeForm}=props;
    const [formData, setFormData] = useState(defaultValue());
    const [formError, setFormError] = useState({

    });

    var comprueba=false;

    const register= () =>{
        let errors = {};
            if( !formData.email || !formData.password || !formData.repeatPassword ){
                if( !formData.email ) errors.email = true;
                if( !formData.password ) errors.password = true;
                if( !formData.repeatPassord ) errors.repeatPassword = true;
            }else if(!validateEmail(formData.email) ){
                    errors.email=true;
            }else if(formData.password !== formData.repeatPassword ){
                    errors.password = true;
                    errors.repeatPassword= true ;
            }else{
                console.log("exito");
                console.log(formData.email);
                console.log(formData.password);
                console.log(formData.repeatPassword);
                comprueba=true;
                console.log(comprueba);
            }
        console.log(formData);
        console.log(errors);
    };

    return(
        <>
            <TextInput
                 style={styles.input}
                 placeholder="Correo electrónico" 
                 placeholderTextColor="#969696"
                 onChange={ (e) => setFormData( {...formData, email: e.nativeEvent.text} ) }
                 
                 />

            <TextInput
                 style={styles.input}
                 placeholder="Contraseña" 
                 placeholderTextColor="#969696"
                 secureTextEntry={true}
                 
                 onChange={ (e) => setFormData( {...formData, password: e.nativeEvent.text} ) }
                 />

            <TextInput
                 style={styles.input}
                 placeholder="Repetir contraseña" 
                 placeholderTextColor="#969696"
                 secureTextEntry={true}

                 onChange={ (e) => setFormData( {...formData, repeatPassword: e.nativeEvent.text} ) }
                 />


            <TouchableOpacity
                onPress={register}>
                { formData.email && formData.password && formData.repeatPassword  ? <Text >  </Text>: <Text style={styles.textoerror}> indroducir datos válidos </Text> }

                
            </TouchableOpacity>


            <View style={styles.login}>
                <TouchableOpacity
                    onPress={changeForm}>
                    <Text style={styles.btnText}>
                        Iniciar sesión 
                    </Text>
                </TouchableOpacity>
            </View>
        </>
    );
}

function defaultValue(){
    return {
        email:'',
        password:'',
        repeatPassword:'',
    };
}

const styles=StyleSheet.create({
    btnText:{
        color: '#fff',
        fontSize: 18
    },

    input:{
        height: 50,
        color: "#fff",
        width: "80%",
        margin: 20,
        backgroundColor: '#1e3040',
        paddingHorizontal: 20,
        borderRadius: 50,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#1e3040',
    },
    login:{
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 10,
    },

    textoerror:{
        height: 50,
        color: "#FF0000",
        width: "100%",
        margin: 10,
        paddingHorizontal: 20,
        borderRadius: 50,
        fontSize: 18,
        borderColor: '#1e3040',     
    }
});