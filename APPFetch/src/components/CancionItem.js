import React from 'react';
import {View, Text, StyleSheet, Button, Alert, Image} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Navigation from '../navigation/Navigation';

const CancionItem= (props) => {

    
    //console.log(props);
    //console.log(navigation);

    const { item , navigation}=props
    //console.log("=================================");
    //console.log({props});
    //console.log(`url -> ${item.album.cover_small}`);
    //console.log(`url -> ${item.album.cover_small}`);

    return (
        <View >
            <TouchableOpacity
                title="hola"
                style={styles.item}
                onPress= {    
                    () => {
                            navigation.navigate("Info", item);
                        }
                    }
                >

                <Image
                    style={styles.estiloImagen}
                    source={ {uri: `${item.album.cover_small}` } } />

                <Text >
                    <Text >
                        Nombre : {`${item.title}`}</Text>
                    <Text >
                        Artista : {`${item.artist.name}`}</Text>
                    <Text >
                        Album : {`${item.album.title}`}</Text>
                    <Text >
                        Link : {`${item.link}`}</Text>
                </Text>
            </TouchableOpacity>
        </View>
    )
} 

export default CancionItem;

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#c2c2c2',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 20
      },

      estiloImagen: {
        textAlign:'left',
        height: 5,
        width: 5,
        padding: 20,
        marginVertical: 3,
        marginHorizontal: 3,
      }
});