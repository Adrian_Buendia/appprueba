import React, { useState }  from 'react';
import {View,Text,StyleSheet ,Image, Button, TouchableOpacity} from 'react-native';
import RegisterForm from './RegisterForm';
import LoginForm from './LoginForm';

export default function Auth(){
    const [isLogin, setIsLogin] = useState(true);

    const changeForm = () => {
        setIsLogin(!isLogin);
    }

    return(
        <View style={styles.view}> 
            <Text> Autenticación </Text>
            <Image 
                style= {styles.logo}
                source={require('../assets/captura.png')}/>
                
            {isLogin ? 
                <LoginForm changeForm={changeForm} /> : 
                <RegisterForm changeForm={changeForm}/> }
        </View>
    );
}


const styles= StyleSheet.create({
    view:{
        flex:1,
        alignItems:"center",
    },

    logo:{
        width:"80%",
        height:"40%",
        marginTop:10,
        marginBottom:10,
    },


});